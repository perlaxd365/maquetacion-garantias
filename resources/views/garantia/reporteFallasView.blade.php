@extends('layouts.plantilla')
@section('title_page', 'Reporte de Fallas')
@section('content_page')


    <?php
    $timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
    $fechaActual = $timestamp->format('Y-m-d');
    
    $fecha2 = date('Y-m-d', strtotime($fechaActual . '- 1 month'));
    
    ?>
    <div class="card">
        <h5 class="card-header bg-light">Criterios de Búsqueda</h5>
        <div class="card-body row">
            <div class="form-row col-12">
                    <div class="card-body row">
                        <div class="form-group col-md-2">
                            <label for="inputEmail4">Desde</label>
                            <input type="date" value="<?php echo $fecha2; ?>" class="form-control date datepicker" id="datepicker"
                                placeholder="01/02/22">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputEmail4">Hasta</label>
                            <input type="date" value="<?php echo $fechaActual; ?>" class="form-control date datepicker" id="datepicker"
                                placeholder="01/02/22">
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label for="inputEmail4">Tipo de Garantía</label>
                            <select onchange="tipoGarantiaSelect(this.value)" class="form-control">
                                <option>Todos</option>
                                <option value="1">Cambio</option>
                                <option value="2">Reparación</option>
                                <option value="3">Nota de Crédito</option>
                            </select>
                        </div>
    
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">Grupo</label>
                            <input type="text" class="form-control" id="inputEmail4" placeholder="Grupo de Producto">
                        </div>
    
                        
                    </div>
                </div>
    
                <div style="padding-top: 5px" class="col-4">
                    <br>
                    <a class="btn btn-primary" href="#"> Buscar</a>
                    <a class="btn btn-primary" href="{{ route('reporteFallasPDF') }}"> Generar Reporte</a>
                    <a class="btn btn-secondary" href="#"> Limpiar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body row">
            <table class="table table-striped  table-hover  default">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ÍTEM</th>
                        <th scope="col">GRUPO</th>
                        <th scope="col">TIPO DE GARANTIA</th>
                        <th scope="col">NRO DE SOLICITUDES</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">1</th>
                        <td>DISCO SOLIDOS</td>
                        <td>CAMBIO</td>
                        <td>3</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">2</th>
                        <td>USB</td>
                        <td>REPARACION</td>
                        <td>5</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">3</th>
                        <td>TARJETAS DE VIDEO</td>
                        <td>NOTA DE CRÉDITO</td>
                        <td>15</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">4</th>
                        <td>DISCO SOLIDOS</td>
                        <td>CAMBIO</td>
                        <td>3</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">5</th>
                        <td>USB</td>
                        <td>REPARACION</td>
                        <td>5</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">6</th>
                        <td>TARJETAS DE VIDEO</td>
                        <td>NOTA DE CRÉDITO</td>
                        <td>15</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">7</th>
                        <td>DISCO SOLIDOS</td>
                        <td>CAMBIO</td>
                        <td>3</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">8</th>
                        <td>USB</td>
                        <td>REPARACION</td>
                        <td>5</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">9</th>
                        <td>TARJETAS DE VIDEO</td>
                        <td>NOTA DE CRÉDITO</td>
                        <td>15</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">10</th>
                        <td>DISCO SOLIDOS</td>
                        <td>CAMBIO</td>
                        <td>3</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">11</th>
                        <td>USB</td>
                        <td>REPARACION</td>
                        <td>5</td>
                    </tr>
                    <tr style="cursor:pointer;" onclick="abrirModal();">
                        <th scope="row">12</th>
                        <td>TARJETAS DE VIDEO</td>
                        <td>NOTA DE CRÉDITO</td>
                        <td>15</td>
                    </tr>
                    <tr style="cursor:pointer;">
                        <th scope="row"></th>
                        <td></td>
                        <td></td>
                        <th scope="row">TOTAL : 95</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>




    <!-- Modal detlle de solicitud -->
    <div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detalle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive table-lg " cellspacing="0">
                        <thead class="thead-dark">
                            <tr style="hover:">
                                <th scope="col">ITEM</th>
                                <th scope="col">DOCUMENTO VENTA</th>
                                <th scope="col">FECHA DOCUMENTO</th>
                                <th scope="col">NRO. SOLICITUD </th>
                                <th scope="col">FECHA INGRESO</th>
                                <th scope="col">CODIGO PRODUCTO</th>
                                <th scope="col">NOMBRE PRODUCTO</th>
                                <th scope="col">MARCA</th>
                                <th scope="col">SERIE</th>
                                <th scope="col">ASIGNADO</th>
                                <th scope="col">USUARIO ASIGNADO</th>
                                <th scope="col">FECHA ASIGNACION</th>
                                <th scope="col">HORA ASIGNACION</th>
                                <th scope="col">FALLA REPORTADA</th>
                                <th scope="col">DETALLE POST REVISIÓN</th>
                                <th scope="col">TIPO DE GARANTÍA (SOLUCION)</th>
                                <th scope="col">ESTADO DE SOLICITUD</th>
                                <th scope="col">FECHA CIERRE</th>
                                <th scope="col">DIAS DE ATENCIÓN</th>
                            </tr>
                        </thead>
                        <tbody>

                            
                    <tr style="cursor:pointer;">
                        <th scope="row">1</th>
                        <td>001-12023</td>
                        <td>03/04/2022</td>
                        <td>000076745</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>DISCO EXT. 1TB CADMIO</td>
                        <td>TOSHIBA</td>
                        <td>XDSV22C3X3</td>
                        <td>SI</td>
                        <td>LUIS ALBERTO</td>
                        <td>03/04/2022</td>
                        <td>01:00</td>
                        <td>NO RECONOCE LA PC</td>
                        <td>SE CORROBORA OFICINA</td>
                        <td>CAMBIO</td>
                        <td>CERRADO</td>
                        <td>04/12/2022</td>
                        <td>2</td>
                    </tr>
                    <tr style="cursor:pointer;">
                        <th scope="row">2</th>
                        <td>001-12023</td>
                        <td>03/04/2022</td>
                        <td>00007656</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>DISCO EXT. 1TB CADMIO</td>
                        <td>TOSHIBA</td>
                        <td>XDSV22C3X3</td>
                        <td>SI</td>
                        <td>LUIS ALBERTO</td>
                        <td>03/04/2022</td>
                        <td>05:00</td>
                        <td>NO RECONOCE LA PC</td>
                        <td>SE CORROBORA OFICINA</td>
                        <td>CAMBIO</td>
                        <td>ESPERA DE REPUESTOS</td>
                        <td>-</td>
                        <td>1</td>
                    </tr>
                    <tr style="cursor:pointer;">
                        <th scope="row">3</th>
                        <td>001-12023</td>
                        <td>03/04/2022</td>
                        <td>00007698</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>DISCO EXT. 1TB CADMIO</td>
                        <td>TOSHIBA</td>
                        <td>XDSV22C3X3</td>
                        <td>SI</td>
                        <td>LUIS ALBERTO</td>
                        <td>03/04/2022</td>
                        <td>02:00</td>
                        <td>NO RECONOCE LA PC</td>
                        <td>SE CORROBORA OFICINA</td>
                        <td>CAMBIO</td>
                        <td>CERRADO</td>
                        <td>04/12/2022</td>
                        <td>4</td>
                    </tr>

                        </tbody>
                    </table>

                    <div class="modal-footer">
                        
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            
            function abrirModal() {
                $('#exampleModalCenter').modal('show');
            }
        </script>
@endsection
