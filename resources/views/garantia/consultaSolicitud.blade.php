@extends('layouts.plantilla')
@section('title_page', 'Consulta de Solicitud de Garantía')
@section('content_page')



    <?php
    $timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
    $fechaActual = $timestamp->format('Y-m-d');
    
    $fecha2 = date('Y-m-d', strtotime($fechaActual . '- 1 month'));
    ?>
    <div class="card">
        <h5 class="card-header bg-light">Consulta de Solicitud de Garantía</h5>
        <div class="card-body row">

            <div class="card col-md-12">
                <div class="card-header">
                    Fecha de Solicitud
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Desde</label>
                        <input type="date" value="<?php echo $fecha2; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Hasta</label>
                        <input type="date" value="<?php echo $fechaActual; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Tipo de Garantía </label>
                        <select  class="form-control">
                                <option>Cambio</option>
                                <option>Reparación</option>
                                <option>Nota de Crédito</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Estado de Solicitud</label>
                        <select  class="form-control">
                            <option disabled>Registrado</option>
                            <option disabled>Recibido en oficina</option>
                            <option>En Revisión</option>
                            <optgroup label="En proceso de atención">
                                <option>Garantía anulada</option>
                                <option>Atención interna</option>
                                <option>Derivado a CAS</option>
                            </optgroup>
                            <option>Cerrado</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Nro. de Solicitud</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                </div>
            </div>
            <div class="card col-12">
                <div class="card-header">
                    Producto
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Código</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Descripción</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Serie</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                </div>
            </div>



            <div style="padding-left: 20px">
                <button type="submit" class="btn btn-primary">Limpiar</button>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body row">
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Ítem</th>
                        <th scope="col">Nro. Solicitud</th>
                        <th scope="col">Código de Producto</th>
                        <th scope="col">Descripción Producto</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Serie</th>
                        <th scope="col">Tipo de Garantía</th>
                        <th scope="col">Estado de Solicitud</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="cursor:pointer; " ondblclick="estadoGarantia (1)">
                        <th scope="row">1</th>
                        <td>00007676</td>
                        <td>00004321</td>
                        <td>DISCO EXT. 1TB CADMIO</td>
                        <td>TOSHIBA</td>
                        <td>XDSV22C3X3</td>
                        <td>CAMBIO</td>
                        <td>RECIBIDO EN OFICINA</td>
                    </tr>
                    <tr style="cursor:pointer;" ondblclick="estadoGarantia (4)">
                        <th scope="row">2</th>
                        <td>00007676</td>
                        <td>00004321</td>
                        <td>IMPRESORA MULTIFUNCIONAL</td>
                        <td>CANNON</td>
                        <td>CDSDF3211</td>
                        <td>REPARACIÓN</td>
                        <td>DERIVADO A CAS</td>
                    </tr>
                    <tr style="cursor:pointer;" ondblclick="estadoGarantia (2)">
                        <th scope="row">3</th>
                        <td>00007676</td>
                        <td>00004321</td>
                        <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                        <td>LENOVO</td>
                        <td>GGDAEWCE32</td>
                        <td>NOTA DE CRÉDITO</td>
                        <td>EN PROCESO DE REVISION</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>



    <script>
        function estadoGarantia(estado) {
            const steps = ['1', '2', '3', '4', '5','6'];
            const estados = ['Registrado', 'Recibido en Oficina', 'En proceso de revisión', 'Garantía Anulada', 'Derivado al CAS',
                'Atención interna'
            ];
            const estadoInicial = estado;
            const swalQueueStep = Swal.mixin({  
                progressSteps: steps
            })

            async function backAndForth() {
                const values = []
                let currentStep

                for (currentStep = estadoInicial; currentStep < steps.length;) {
                    const result = await swalQueueStep.fire({
                        title: estados[currentStep],
                        text: "03/04/2022",
                        inputValue: values[currentStep],
                        currentProgressStep: currentStep
                    })

                    if (result.value) {
                        break
                    }
                }

                if (currentStep === steps.length) {
                    Swal.close();
                }
            }

            backAndForth()
        }
    </script>
@endsection
