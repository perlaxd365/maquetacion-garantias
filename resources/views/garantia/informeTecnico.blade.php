@extends('layouts.plantilla')
@section('title_page', 'Registrar Proceso de Garantía')
@section('content_page')
    <div class="card">
        <h5 class="card-header bg-light">Detalle de Producto</h5>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Código</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="00000001"
                        placeholder="00000001">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Color</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="BLANCO" placeholder="BLANCO">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Marca</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="LENOVO" placeholder="LENOVO">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Serie/IMEI</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="XS324F4221D2"
                        placeholder="XS324F4221D2">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Modelo</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="GAMER" placeholder="GAMER">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Serie-Número de Venta</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="001-000012"
                        placeholder="001-000012">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Fecha de Documento de Venta</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="12/04/2022"
                        placeholder="12/04/2022">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Tiempo de Garantía</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="1 año" placeholder="1 año">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Fin de Garantía</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="12/04/2023"
                        placeholder="12/04/2023">
                </div>
            </div>


        </div>
    </div>
    <div class="card">
        <h5 class="card-header bg-light">Detalle de Falla</h5>
        <div class="card-body">
            <div class="form-row">
                <textarea class="form-control" cols="5" rows="5"
                    placeholder="El equipo enciende pero no es reconocido por el cable USB, al momento de conectar al computador"></textarea>
            </div>

        </div>
    </div>
    <div class="card">
        <h5 class="card-header bg-light">Accesorios</h5>
        <div class="card-body">
            <div class="form-row">
                <textarea class="form-control" cols="5" rows="5" placeholder="Se envía el cable de poder y cable USB 3.0"></textarea>
            </div>

        </div>
    </div>
    <div class="card">
        <h5 class="card-header bg-light">Imágenes Adjuntas</h5>
        <div class="card-body row row-cols-sm-2 center-block" style=" display: flex;
                                            justify-content: center;
                                            align-items: center;">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <style>
                    img.relacionaspecto {
                        height: 50%;
                        width: 50%;
                        top: 0;
                        left: 0;
                    }

                    img.relacionaspecto {
                        aspect-ratio: 4/3;
                        width: 50%;
                        object-fit: cover;
                    }

                </style>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img style="border-radius: 15px;" class="d-block w-100 relacionaspecto"
                            src="https://reparandoloec.com/wp-content/uploads/2020/10/ubicacion-amperaje.jpeg"
                            alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img style="border-radius: 15px;" class="d-block w-100 relacionaspecto"
                            src="https://elcomercio.pe/resizer/QiBMC1B2VuT1FOP6ZT_OGTiUiOE=/980x0/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/2Y44WZP6GJCJ3JFR2LKO73HNAU.jpg"
                            alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img style="border-radius: 15px;" class="d-block w-100 relacionaspecto"
                            src="https://img.freepik.com/foto-gratis/laptop-plateada-compacta-pantalla-negra-apagada-sentada-estante-dos-cactus-macetas-marrones-visto-cerca-angulo_126745-2432.jpg?w=1060"
                            alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </div>
    <div class="card">
        <h5 class="card-header bg-light">Detalle Interno</h5>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Técnico</label>
                    <input type="text" readonly class="form-control" id="inputEmail4" value="JUAN PEREZ"
                        placeholder="JUAN PEREZ">
                </div>
                <div class="form-group col-md-12">
                    <label for="inputEmail4">Diagnóstico del Técnico</label>
                    <div class="form-row">
                        <textarea class="form-control" cols="5" rows="5"></textarea>
                    </div>

                </div>


            </div>


        </div>
    </div>

    <div class="card">
        <h5 class="card-header bg-light">Estado de Atención de Garantía</h5>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group row col-md-12">
                    <div class="col-md-2">
                        <label for="inputEmail4">Estado de Solicitud</label>
                        <select onchange="tipoGarantiaDiv(this);" class="form-control">
                            <option disabled>Registrado</option>
                            <option disabled>Recibido en oficina</option>
                            <option>En Revisión</option>
                            <optgroup label="En proceso de Atención">
                                <option>Garantía Anulada</option>
                                <option value="Atención Interna">Atención Interna</option>
                                <option value="Derivado al CAS">Derivado al CAS</option>
                            </optgroup>
                            <option>Cerrado</option>
                        </select>

                    </div>
                    <div id="tipodeGarantiaDiv" class="form-group col-md-2" hidden>
                        <label for="inputEmail4">Seleccionar Tipo de Garantía</label>
                        <select onchange="tipoGarantiaSelect(this.value)" class="form-control">
                            <option>Por Definir</option>
                            <option value="1">Cambio</option>
                            <option value="2">Reparación</option>
                            <option value="3">Nota de Crédito</option>
                        </select>
                    </div>
                    <div id="estadoSolicitudDiv" class="form-group col-md-2" hidden>
                        <label for="inputEmail4">Estado de Solicitud</label>
                        <select class="form-control">
                        </select>
                    </div>
                    <div id="descripcionEstado" class="form-group col-md-3" hidden>
                        <label for="inputEmail4">Descripción</label>
                        <input type="text" class="form-control" id="inputEmail4"
                            placeholder="Se inició el proceso de Garantía">
                    </div>
                    <div class="col-2 row" style="padding-top: 30px; padding-bottom: 30px;">
                        <button class="btn btn-outline-primary my-2 my-sm-0" onclick="guardar();"
                            type="submit">Agregar</button>
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Quitar</button>
                    </div>
                </div>
                <div class="form-group  col-md-12">
                    <table class="table ">
                        <thead class="">
                            <tr class="table-secondary">
                                <th scope="col">Estado</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">Fecha y Hora</th>
                                <th scope="col">Descripción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Caso cerrado</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>17/03/2022 06:40</td>
                                <td>Se completó la entrega del dispositivo</td>
                            </tr>
                            <tr>
                                <th scope="row">Atención Interna/Reparación/Reparación concluida</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>15/03/2022 06:40</td>
                                <td>Se finalizó con éxito</td>
                            </tr>
                            <tr>
                                <th scope="row">Atención Interna/Reparación/En reparación</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>11/03/2022 02:20</td>
                                <td>Ensamblando nuevos componentes</td>
                            </tr>
                            <tr>
                                <th scope="row">Atención Interna/Reparación/En espera de repuestos</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>10/03/2022 03:10</td>
                                <td>Esperando repuestos para el equipo "GALAXY S22 ULTRA"</td>
                            </tr>
                            <tr>
                                <th scope="row">En Revisión</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>15/03/2022 06:40</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th scope="row">Recibido en oficina</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>11/03/2022 02:20</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th scope="row">Registrado</th>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>10/03/2022 03:10</td>
                                <td>-</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <hr width=1500>
                <h5>Registro de actividades</h5>
                <div id="tipodeGarantiaTable" class="form-group col-md-12">
                    <div class="row col-md-12">
                        <div id="descripcionEstado" class="form-group col-md-3">
                            <label for="inputEmail4">Descripción</label>
                            <input type="text" class="form-control" id="inputEmail4"
                                placeholder="Se inició el proceso de Garantía">
                        </div>
                        <div class="form-group col-md-3" style="padding-top: 30px; padding-bottom: 30px;">
                            <button class="btn btn-outline-primary my-2 my-sm-0" onclick="guardar();"
                                type="submit">Agregar</button>
                            <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Quitar</button>
                        </div>
                    </div>
                    <table class="table">
                        <thead class="">
                            <tr class="table-secondary">
                                <th scope="col">Item</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">Fecha y Hora</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">1</td>
                                <td>Finalizando reparació con éxito</td>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>17/03/2022 06:40</td>
                            </tr>
                            <tr>
                                <td  scope="row">2</td>
                                <td>Aplicando reparación</td>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>15/03/2022 06:40</td>
                            </tr>
                            <tr>
                                <td  scope="row">3</td>
                                <td>En revisión</td>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>11/03/2022 02:20</td>
                            </tr>
                            <tr>
                                <td  scope="row">4</td>
                                <td>Se inició el informe técnico</td>
                                <td>JUAN ALFREDO CORREA</td>
                                <td>10/03/2022 03:10</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div class="alert alert-warning" role="alert">
                    En cada actualización, se le notificará al Usuario el estado en que se encuentra su producto !!
                </div>
                <div class="form-group col-md-12">
                    <label for="inputEmail4">Detalle post revisión</label>
                    <div class="form-row">
                        <textarea class="form-control" cols="5" rows="5"></textarea>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="mx-auto col" style="width: 450px;">
        <button type="button" onclick="EnviarForm()" class="btn btn-primary">Guardar Informe Técnico</button>
        <a class="btn btn-secondary" href="{{ route('garantia.consultaSolicitudEjecutar') }}">Cancelar</a>
    </div>
    <br><br>

    <script>
        function EnviarForm() {

            Swal.fire({
                title: 'Estas seguro que quieres registrar Informe Técnico',
                type: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: `Cancelar`,
            }).then((result) => {

                Swal.fire('Informe Técnico Registrado', '', 'success').then((result) => {
                    window.location = "{{ route('DownloadPdf') }}";
                })

            })
        }

        function actualizarBitacora() {

            Swal.fire({
                title: 'Estas seguro que quieres actualizar la bitácora',
                type: 'warning',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: `Cancelar`,
            }).then((result) => {

                Swal.fire('Bitácora actualizada', '', 'success').then((result) => {
                    window.location = "{{ url('garantia/informeTecnico') }}";
                })

            })
        }

        function tipoGarantiaSelect(option) {
            var select;
            switch (option) {
                case option = '1':

                    var select =
                        "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>Autorización de cambio</option> <option>Generación de nota crédito</option> <option>Emisión factura</option></select>";

                    break;
                case option = '2':

                    var select =
                        "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>En espera de repuestos</option> <option>En reparación</option><option>Reparación concluida</option></select>";

                    break;
                case option = '3':
                    var select =
                        "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>Autorización nota crédito</option> <option> Nota crédito emitida</option></select>";

                default:
                    break;
            }

            let formTipoGarantia = document.getElementById('estadoSolicitudDiv');
            formTipoGarantia.removeAttribute("hidden");
            let descripcionEstado = document.getElementById('descripcionEstado');
            descripcionEstado.removeAttribute("hidden");
            $("#estadoSolicitudDiv").attr("disabled", false);
            $("#estadoSolicitudDiv").html(select);
        }

        function tipoGarantiaDiv(val) {
            var texto = val.value;
            if (texto == 'Atención Interna' || texto == 'Derivado al CAS') {
                document.getElementById('tipodeGarantiaDiv').hidden = false;
            } else {
                document.getElementById('tipodeGarantiaDiv').hidden = true;
                document.getElementById('estadoSolicitudDiv').hidden = true;
                document.getElementById('descripcionEstado').hidden = true;
            }
        }

        function guardar() {

            Swal.fire('Se guardó correctamente', '', 'success').then((result) => {})
        }
    </script>
@endsection
