@extends('layouts.plantilla')
@section('title_page', 'Consulta de Solicitud de Garantía por Ejecutar')
@section('content_page')
    <script>
        $(function() {
            $('tr').click(function(e) {
                if ($(this).hasClass('row-selected')) {
                    $(this).addClass('other-clic')
                } else {
                    cleanTr()
                    $(this).addClass('row-selected')
                }
            })


            function cleanTr() {
                $('.row-selected').each(function(index, element) {
                    $(element).removeClass('row-selected')
                    $(element).removeClass('other-clic')
                })
            }
        })
    </script>
    <style>
        .row-selected {
            background: yellow;
        }


        .other-clic {
            background: green !important;
            color: white;
        }

        tr {
            cursor: pointer;
        }

    </style>

    <?php
    $timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
    $fechaActual = $timestamp->format('Y-m-d');
    
    $fecha2 = date('Y-m-d', strtotime($fechaActual . '- 1 month'));
    ?>
    <div class="card">
        <h5 class="card-header bg-light">Consulta de Solicitud de Garantía</h5>
        <div class="card-body row">

            <div class="card col-md-12">
                <div class="card-header">
                    Fecha de Solicitud
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Desde</label>
                        <input type="date" value="<?php echo $fecha2; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Hasta</label>
                        <input type="date" value="<?php echo $fechaActual; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group row col-md-4">
                        <div class="col-9">
                            <label for="inputEmail4">Cliente</label>
                            <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar">

                        </div>
                        <div class="col-3" style="padding-top: 30px">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>

                        </div>
                    </div>


                    <div class="form-group col-md-2">
                        <label for="inputEmail4">RUC/DNI</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Serie de Venta</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Número de Venta</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>

                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Tipo de Garantía</label>
                        <select onchange="tipoGarantiaSelect(this.value)" class="form-control">
                            <option>Por Definir</option>
                            <option value="1">Cambio</option>
                            <option value="2">Reparación</option>
                            <option value="3">Nota de Crédito</option>
                        </select>
                    </div>
                    <div id="estadoSolicitudDiv" class="form-group col-md-3">
                        <label for="inputEmail4">Estado de Solicitud</label>
                        <select class="form-control">
                            <option>En proceso</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Serie de Producto</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Descripción de Producto</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Asignado</label>
                        <select class="form-control">
                            <option>SI</option>
                            <option>NO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Usuario Asignado</label>
                        <select id="tipo_documentos" class="form-control">
                            <option>LUIS ALBERTO</option>
                            <option>JAIVER APARICIO</option>
                            <option>JUAN CORREA</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Nro. de Solicitud</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Tipo de Solicitud</label>
                        <select class="form-control">
                            <option>Interna</option>
                            <option>Externa</option>
                        </select>
                    </div>
                </div>
            </div>





            <div style="padding-left: 20px">
                <button type="submit" class="btn btn-primary">Limpiar</button>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body row">
            <table class="table table-striped table-hover  default">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Ítem</th>
                        <th scope="col">Nro. Solicitud</th>
                        <th scope="col">Fecha de Ingreso</th>
                        <th scope="col">RUC</th>
                        <th scope="col">Razón Social</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="cursor:pointer;">
                        <th scope="row">1</th>
                        <td>00007676</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>HIRAOKA</td>
                        <td><button class="btn btn-primary" onclick="abrirModal();">Ver Detalle</button></td>
                    </tr>
                    <tr style="cursor:pointer;">
                        <th scope="row">2</th>
                        <td>00007676</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>HIRAOKA</td>
                        <td><button class="btn btn-primary" onclick="abrirModal();">Ver Detalle</button></td>
                    </tr>
                    <tr selected style="cursor:pointer;">
                        <th scope="row">3</th>
                        <td>00007676</td>
                        <td>03/04/2022</td>
                        <td>20324929311</td>
                        <td>HIRAOKA</td>
                        <td><button class="btn btn-primary" onclick="abrirModal();">Ver Detalle</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>




    <!-- Modal detlle de solicitud -->
    <div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detalle de Solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive table-lg " cellspacing="0">
                        <thead class="thead-dark">
                            <tr style="hover:">
                                <th scope="col"></th>
                                <th scope="col">Código de Producto</th>
                                <th scope="col">Descripción de Producto</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Serie</th>
                                <th scope="col">Asignado</th>
                                <th scope="col">Usuario Asignado</th>
                                <th scope="col">Fecha de Asignacion</th>
                                <th scope="col">Hora de Asignacion</th>
                                <th scope="col">Tipo de Garantía</th>
                                <th scope="col">Estado de Garantía</th>


                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="radio" onclick="verBoton (this);" id="garantia" name="garantia" value="SI"
                                        aria-label="Checkbox for following text input"></td>
                                <td>XDSV22C3X3</td>
                                <td>DISCO EXT. 1TB CADMIO</td>
                                <td>TOSHIBA</td>
                                <td>X8239F2C9C3932</td>
                                <td>SI</td>
                                <td>LUIS ALBERTO</td>
                                <td>03/04/2022</td>
                                <td>12:30</td>
                                <td>CAMBIO</td>
                                <td>AUTORIZACIÓN DE CAMBIO</td>
                            </tr>
                            <tr>
                                <td><input type="radio" onclick="verBoton (this);" id="garantia" name="garantia" value="SI"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>SI</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>03:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>
                            <tr>
                            <tr selected style="cursor:pointer;">
                                <td><input type="radio" onclick="verBoton (this);" id="garantia" name="garantia" value="NO"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>NO</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>03:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>
                            <tr>
                                <td><input type="radio" onclick="verBoton (this);" id="garantia" name="garantia" value="NO"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>NO</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>02:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>

                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <button type="submit" id="botonIniciar" hidden="hidden" class="btn btn-primary"
                            onclick="tipoGarantia()">Iniciar Proceso </button>

                        <button class="btn btn-secondary" onclick="vinculacion();">Vincular</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function tipoGarantia() {

                window.location = "{{ url('garantia/informeTecnico') }}";


            }

            function vinculacion() {
                $("#exampleModalCenter").modal('hide');
                var nroSolicitud, fecha, tipoDoc, nroDoc;
                Swal.fire({
                    title: 'Vinculación de Documento',
                    html: '<div class="panel panel-default"> <div class="panel-body"> DISCO EXT. 1TB CADMIO | TOSHIBA | HIRAOKA </div> </div><br>' +
                        '<label style="float:left;">Nro Solicitud</label><input type="text" id="nro_solicitud" class="form-control"><br>' +
                        '<label style="float:left;">Fecha Solicitud</label><input type="date"  class="form-control date datepicker" id="fecha_solicitud" value="2022-01-02"><br>' +
                        '<label style="float:left;">Tipo de Documento</label><select id="tipo_documento" class="form-control"><option>Documento de Compra</option><option>Nota de Crédito</option><option>Otro</option></select><br>' +
                        '<label style="float:left;">Nro Documento</label><input type="text"  id="nro_documento" class="form-control"><br>' +
                        '<label style="float:left;">Adjuntar Documentos</label><input type="file"  id="nro_documento" class="form-control"><br>' +
                        '<label style="float:left;">Descripción</label><textarea cols="5" rows="5" class="form-control" placeholder="Agrega un comentario"></textarea><br>',
                    focusConfirm: false,
                    preConfirm: () => {
                        return [
                            nro_solicitud = document.getElementById('nro_solicitud').value,
                            fecha_solicitud = document.getElementById('fecha_solicitud').value,
                            tipo_documento = document.getElementById('tipo_documento').value,
                            nro_documento = document.getElementById('nro_documento').value
                        ]
                    }
                }).then(function(value) {
                    Swal.fire('Vinculación exitosa!', 'Se añadió a la solicitud de Garantía <br> SOLICITUD: ' +
                        nro_solicitud + '<br>  FECHA: ' + fecha_solicitud + '<br>  TIPO DE DOC: ' + tipo_documento +
                        ' <br>  NRO DOC: ' + nro_documento + '', 'success').then(function() {
                        window.location.reload()
                    })
                })

                if (formValues) {
                    Swal.fire(JSON.stringify(formValues))
                }
            }

            function abrirModal() {
                $('#exampleModalCenter').modal('show');
            }

            function verBoton(valor) {
                if (valor.value == "SI") {

                    var botonIniciar = document.getElementById('botonIniciar');
                    botonIniciar.innerText = "Continuar Proceso";
                    botonIniciar.hidden = false;
                } else if (valor.value == "NO") {

                    var botonIniciar = document.getElementById('botonIniciar');
                    botonIniciar.innerText = "Iniciar Proceso";
                    botonIniciar.hidden = false;
                }
            }

            function CierraPopup() {
                $("#exampleModalCenter").modal('hide');
            }

        </script>
    @endsection
