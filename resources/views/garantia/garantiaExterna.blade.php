@extends('layouts.plantilla')
@section('title_page', 'Consulta de Documento')
@section('content_page')



    <div class="card">
        <h5 class="card-header bg-light">Entidad</h5>
        <div class="card-body">

            <div class="form-group row col-md-12">
                <div class="col-md-4">
                    <label for="inputEmail4">Establecimiento/Tienda Comercial</label>
                    <select class="form-control">
                        <option>HIRAOKA</option>
                        <option>LA CURACAO</option>
                    </select>

                </div>
            </div>

        </div>

        <div class="card">
            <h5 class="card-header bg-light">Datos Personales (Notificación)</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Nombres y Apellidos</label>
                        <input class="form-control mr-sm-2" type="text" placeholder="Introduce tus nombres"
                            aria-label="COD-0001">

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Correo Electrónico</label>
                        <input class="form-control mr-sm-2" type="email" placeholder="Introduce un email"
                            aria-label="COD-0001">

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">DNI</label>
                        <input class="form-control mr-sm-2" type="text" placeholder="10932923" aria-label="COD-0001">

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Teléfono</label>
                        <input class="form-control mr-sm-2" type="text" placeholder="943234319" aria-label="COD-0001">

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Dirección</label>
                        <input class="form-control mr-sm-2" type="text" placeholder="Urb. Bruces MZ 1 LT 2"
                            aria-label="COD-0001">

                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <h5 class="card-header bg-light">Detalle de Producto</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Código</label>
                        <input class="form-control  mr-sm-2" type="search" placeholder="COD-0001" aria-label="COD-0001">

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Color</label>
                        <input type="text" class="form-control  mr-sm-2" id="inputEmail4" value="" placeholder="BLANCO">
                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Marca</label>
                        <input type="text" class="form-control  mr-sm-2" id="inputEmail4" value="" placeholder="TOSHIBA">
                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Serie/Imei</label>
                        <input class="form-control   mr-sm-2" type="search" placeholder="0293924932923"
                            aria-label="0293924932923">
                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Modelo</label>
                        <input type="text" class="form-control  mr-sm-2" id="inputEmail4" value="" placeholder="GAMER">
                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Tipo de Comprobante</label>
                        <select  class="form-control  mr-sm-2" name="" id="">
                            <option value="">Factura</option>
                            <option value="">Boleta</option>
                        </select>

                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Serie de Comprobante</label>
                        <input type="text" class="form-control  mr-sm-2" id="inputEmail4" value="" placeholder="001">
                    </div>
                    <div class="form-group row col-md-4">
                        <label for="inputEmail4">Número de Comprobante</label>
                        <input type="text" class="form-control  mr-sm-2" id="inputEmail4" value="" placeholder="1423431">
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <h5 class="card-header bg-light">Detalle de Falla</h5>
            <div class="card-body">
                <div class="form-row">
                    <textarea class="form-control" cols="5" rows="5"
                        placeholder="El equipo enciende pero no es reconocido por el cable USB, al momento de conectar al computador"></textarea>
                </div>

            </div>
        </div>
        <div class="card">
            <h5 class="card-header bg-light">Accesorios</h5>
            <div class="card-body">
                <div class="form-row">
                    <textarea class="form-control" cols="5" rows="5" placeholder="Se envía el cable de poder y cable USB 3.0"></textarea>
                </div>

            </div>
        </div>
        <div class="card">
            <h5 class="card-header bg-light">Ajuntar evidencia (Doumentos o Imágenes)</h5>
            <div class="card-body">
                <div class="form-row">
                    <input type="file" class="form-control">
                </div>

            </div>
        </div>
        <div class="mx-auto" style="width: 250px;">
            <button type="button" onclick="EnviarForm()" class="btn btn-primary">Guardar Solicitud de Garantía</button>
        </div>
        <br>
        <br>
        <script>
            function EnviarForm() {
                Swal.fire({
                    title: 'Confirmar Registro',
                    text: '¿Estás seguro que deseas registrar la solicitud?',
                    type: 'warning',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: `Cancelar`,
                }).then((result) => {
                    Swal.fire('Solicitud registrada!', '', 'success')
                })
            }
        </script>
    @endsection
