@extends('layouts.plantilla')
@section('title_page', 'Detalle de Solicitud')
@section('content_page')

    <div class="card">
        <h5 class="card-header bg-light">Detalle del Producto</h5>
        <div class="card-body">

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Código</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="000095842">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Color</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="NEGRO">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Marca</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="TOSHIBA">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Serie/IMEI</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="XD4WG333C">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Modelo</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="AA3D3DD6">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Doc. Compra</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="F001-3344433">
                </div>
            </div>
        </div>
    </div>

    
    <div class="card">
        <h5 class="card-header bg-light">Detalle de Falla</h5>
        <div class="card-body">
            <textarea class="form-control" id="" cols="5" rows="5" placeholder="El equipo enciende, pero no es reconocido por el cable usb al momendo de conectar al computador"></textarea>
        </div>
    </div>
    
    <div class="card">
        <h5 class="card-header bg-light">Accesorios</h5>
        <div class="card-body">
            <textarea class="form-control" id="" cols="5" rows="5"placeholder="Se envía el cable de poder y cable usb 3.0"></textarea>
        </div>
    </div>

    <div class="card">
        <h5 class="card-header bg-light">Imágenes Adjuntas (6 como máximo)</h5>
        <div class="card-body">
            <input type="file">
        </div>
    </div>

    <div class="mx-auto" style="width: 250px;"> 
        <a href="{{url('garantia/registroSolicitud')}}"   class="btn btn-primary">Adicionar Producto a Solicitud</a>
    </div>
    <br>
@endsection
