@extends('layouts.plantilla')
@section('title_page', 'Registro de solicitud')
@section('content_page')
<?php
$timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
$fecha=$timestamp->format('Y-m-d');
$hora=$timestamp->format('H:i');

?>
    <form action="" enctype="multipart/form-data">
        <div class="card">
            <h5 class="card-header bg-light">Datos de Cliente</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Nro. de Solicitud (Autogenerado)</label>
                        <input type="text" readonly class="form-control" id="inputEmail4" value="00000001" placeholder="000323202">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Fecha</label>
                        <input type="date" class="form-control date datepicker" readonly value="<?php echo $fecha;?>" id="datepicker" placeholder="Email">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Hora</label>
                        <input type="time" class="form-control" placeholder="Email" readonly value="<?php echo $hora;?>">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Usuario</label>
                        <input type="text" class="form-control" id="inputEmail4" value="JPEREZ" readonly placeholder="JPEREZ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Ruc</label>
                        <input type="text" class="form-control" value="102373623526" readonly id="inputEmail4" placeholder="20100016681">
                    </div>
                    <div class="form-group col-md-9">
                        <label for="inputEmail4">Razón Social</label>
                        <input type="text" class="form-control" value="IMPORTACIONES HIRAOKA S.A.C." readonly id="inputEmail4"
                            placeholder="IMPORTACIONES HIRAOKA S.A.C.">
                    </div>
                </div>


            </div>
        </div>

        <div class="card">
            <h5 class="card-header bg-light">Datos de Notificación del Cliente</h5>
            <div class="card-body">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Correo Electrónico</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Número Celular</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="inputPassword3" placeholder="Móvil">
                    </div>
                </div>
            </div>
        </div>
        <div class="mx-auto" style="padding-left: 20px">
            <a href = "{{ url('garantia/consultaDocumento') }}" class="btn btn-primary">Añadir Producto</a>
            <button type="submit" class="btn btn-primary">Eliminar Producto</button>
        </div>
        <br>
        <div class="card">
            <div class="card-body row">
                <table  class="table table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Fecha Despacho</th>
                            <th scope="col">Documento Depacho</th>
                            <th scope="col">Fecha Límite Garantía</th>
                            <th scope="col">Documento (Serie - Número)</th>
                            <th scope="col">Codigo Producto</th>
                            <th scope="col">Descripción Producto</th>
                            <th scope="col">Serie</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                            <td>11/04/2022</td>
                            <td>0000124123</td>
                            <td>11/07/2022</td>
                            <td>000-0000123</td>
                            <td>PRO-001</td>
                            <td>Laptop Dell Ryzen 5 15.6" 8GB 256GB SSD</td>
                            <td>S-41-1</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" aria-label="Checkbox for following text input"></td>
                            <td>11/04/2022</td>
                            <td>0000124123</td>
                            <td>11/07/2022</td>
                            <td>000-0000123</td>
                            <td>PRO-002</td>
                            <td>LAPTOP DELL LATITUDE 3400 INTEL CORE I5 8GB 1TB 14"</td>
                            <td>S-44-3</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" aria-label="Checkbox for following text input"></td> 
                            <td>11/04/2022</td>
                            <td>0000124123</td>
                            <td>11/07/2022</td>
                            <td>000-0000123</td>
                            <td>PRO-003</td>
                            <td>Dell Inspiron 15 3000 Business Laptop, 15.6"</td>
                            <td>S-121-2</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mx-auto" style="width: 250px;">
            <button type="button" onclick="EnviarForm()" class="btn btn-primary">Guardar Solicitud de Garantía</button>
        </div>
    </form>
    <br>

    <script>
        function EnviarForm() {
            Swal.fire({
                title: 'Confirmar Registro',
                text: '¿Estás seguro que deseas registrar la solicitud?',
                type:'warning',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: `Cancelar`,
            }).then((result) => {
                Swal.fire('Solicitud registrada!', '', 'success')
            })
        }
    </script>
@endsection
