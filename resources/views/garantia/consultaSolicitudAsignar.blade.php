@extends('layouts.plantilla')
@section('title_page', 'Consulta de Solicitud de Garantía por Asignar')
@section('content_page')
    <script>
        $(function() {
            $('tr').click(function(e) {
                if ($(this).hasClass('row-selected')) {
                    $(this).addClass('other-clic')
                } else {
                    cleanTr()
                    $(this).addClass('row-selected')
                }
            })


            function cleanTr() {
                $('.row-selected').each(function(index, element) {
                    $(element).removeClass('row-selected')
                    $(element).removeClass('other-clic')
                })
            }
        })
    </script>
    <style>
        .row-selected {
            background: yellow;
        }


        .other-clic {
            background: green !important;
            color: white;
        }

        tr {
            cursor: pointer;
        }

    </style>

    <?php
    $timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
    $fechaActual = $timestamp->format('Y-m-d');
    
    $fecha2 = date('Y-m-d', strtotime($fechaActual . '- 1 month'));
    ?>
    <div class="card">
        <h5 class="card-header bg-light">Consulta de Solicitud de Garantía</h5>
        <div class="card-body row">

            <div class="card col-md-12">
                <div class="card-header">
                    Fecha de Solicitud
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Desde</label>
                        <input type="date" value="<?php echo $fecha2; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Hasta</label>
                        <input type="date" value="<?php echo $fechaActual; ?>" class="form-control date datepicker" id="datepicker"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group row col-md-4">
                        <div class="col-9">
                            <label for="inputEmail4">Cliente</label>
                            <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar">

                        </div>
                        <div class="col-3" style="padding-top: 30px">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>

                        </div>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Tipo de Garantía</label>
                        <select onchange="tipoGarantiaSelect(this.value)" class="form-control">
                            <option>Por Definir</option>
                            <option value="1">Cambio</option>
                            <option value="2">Reparación</option>
                            <option value="3">Nota de Crédito</option>
                        </select>
                    </div>
                    <div id="estadoSolicitudDiv" class="form-group col-md-3">
                        <label for="inputEmail4">Estado de Solicitud</label>
                        <select class="form-control">
                            <option>En proceso</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">RUC/DNI</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Serie de Venta</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Número de Venta</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>


                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Serie de Producto</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Descripción de Producto</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Nro. de Solicitud</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputEmail4">Asignado</label>
                        <select class="form-control">
                            <option>SI</option>
                            <option>NO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Tipo de Solicitud</label>
                        <select class="form-control">
                            <option>Interna</option>
                            <option>Externa</option>
                        </select>
                    </div>
                </div>
            </div>





            <div style="padding-left: 20px">
                <button type="submit" class="btn btn-primary">Limpiar</button>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">

            <div class="card-header ">
                <h6 class="font-weight-bold">Leyenda de Solicitud</h6>
                <div class="row">


                    <br>
                    <div class="p-1 mb-2 table-success">Atendido</div>
                    <div class="p-1 mb-2 table-warning ">Parcialmente atendido</div>
                    <div class="p-1 mb-2 table-danger">Sin Atender</div>
                </div>
                <br>
            <div class="col-12">

                <table class="table table-striped table-hover default">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Ítem</th>
                            <th scope="col">Nro. Solicitud</th>
                            <th scope="col">Fecha de Ingreso</th>
                            <th scope="col">RUC</th>
                            <th scope="col">Razón Social</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-success" onclick="abrirModal();" style="cursor:pointer;" >
                            <th scope="row">1</th>
                            <td>00007676</td>
                            <td>03/04/2022</td>
                            <td>20324929311</td>
                            <td>HIRAOKA</td>
                        </tr>
                        <tr class="table-success" onclick="abrirModal();" style="cursor:pointer;">
                            <th scope="row">2</th>
                            <td>00007676</td>
                            <td>03/04/2022</td>
                            <td>20324929311</td>
                            <td>HIRAOKA</td>
                        </tr>
                        <tr class="table-warning" onclick="abrirModal();" style="cursor:pointer;">
                            <th scope="row">3</th>
                            <td>00007676</td>
                            <td>03/04/2022</td>
                            <td>20324929311</td>
                            <td>HIRAOKA</td>
                        </tr>
                        <tr class="table-danger" onclick="abrirModal();" style="cursor:pointer;">
                            <th scope="row">4</th>
                            <td>00007676</td>
                            <td>03/04/2022</td>
                            <td>20324929311</td>
                            <td>HIRAOKA</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <!-- Modal detlle de solicitud -->
    <div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detalle de Solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive table-lg " cellspacing="0">
                        <thead class="thead-dark">
                            <tr style="hover:">
                                <th scope="col"></th>
                                <th scope="col">Código de Producto</th>
                                <th scope="col">Descripción de Producto</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Serie</th>
                                <th scope="col">Asignado</th>
                                <th scope="col">Usuario Asignado</th>
                                <th scope="col">Fecha de Asignacion</th>
                                <th scope="col">Hora de Asignacion</th>
                                <th scope="col">Tipo de Garantía</th>
                                <th scope="col">Estado de Garantía</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="checkbox" name="garantia" value="SI"
                                        aria-label="Checkbox for following text input"></td>
                                <td>XDSV22C3X3</td>
                                <td>DISCO EXT. 1TB CADMIO</td>
                                <td>TOSHIBA</td>
                                <td>X8239F2C9C3932</td>
                                <td>SI</td>
                                <td>LUIS ALBERTO</td>
                                <td>03/04/2022</td>
                                <td>12:30</td>
                                <td>CAMBIO</td>
                                <td>AUTORIZACIÓN DE CAMBIO</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="garantia" value="SI"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>SI</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>03:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>
                            <tr>
                            <tr selected style="cursor:pointer;">
                                <td><input type="checkbox" name="garantia" value="NO"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>NO</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>03:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="garantia" value="NO"
                                        aria-label="Checkbox for following text input"></td>
                                <td>GGDAEWCE32</td>
                                <td>LAPTOP LENOVO iCore 5TH. 8 GB RAM 256 SSD</td>
                                <td>LENOVO</td>
                                <td>X8239F2C9C3932</td>
                                <td>NO</td>
                                <td>JUAN CORREA</td>
                                <td>03/04/2022</td>
                                <td>02:30</td>
                                <td>NOTA DE CRÉDITO</td>
                                <td>NOTA EMITIDA</td>
                            </tr>

                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" onclick="tipoGarantia ()">Asignar Garantía</button>
                        <button type="submit" class="btn btn-primary" onclick="quitarAsignacion ()">Quitar
                            Asignación</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function tipoGarantia() {
                var checkboxSeleccionados = $("input:checkbox:checked").map(function() {
                    return $(this).val();
                }).get();

                if (checkboxSeleccionados.length < 1) {

                    Swal.fire('Por favor selecciona uno o mas items', '',
                        'warning')
                } else {


                    var contador = 0;
                    var arrTodo = new Array();
                    var validar = true;
                    for (let index = 0; index < checkboxSeleccionados.length; index++) {
                        contador++;
                        var item = {};
                        item = checkboxSeleccionados[index];
                        arrTodo.push(item);
                        if (item == "SI") {
                            validar = false;
                        }
                    }

                    if (validar == true) {

                        var toPost = JSON.stringify(arrTodo);
                        console.log(toPost);
                        Swal.fire({
                            title: 'Asignar Garantía',
                            input: 'select',
                            inputOptions: {
                                'LUIS ALBERTO': 'LUIS ALBERTO',
                                'JAIVER APARICIO': 'JAIVER APARICIO',
                                'JUAN CORREA': 'JUAN CORREA',
                            },
                            text: 'Seleccione el Trabajador',
                            showCancelButton: true,
                            inputValidator: (value) => {
                                return new Promise((resolve) => {

                                    Swal.fire('Se asignó (' + contador + ') garantías a ' + value, '',
                                        'success').then((result) => {
                                        location.reload();
                                    })


                                })
                            }
                        })
                    } else {
                        Swal.fire('Se encontró un item con garantía asignada, por favor vuelve a intentarlo', '',
                            'warning')
                    }

                }
            }

            function quitarAsignacion() {

                var checkboxSeleccionados = $("input:checkbox:checked").map(function() {
                    return $(this).val();
                }).get();




                if (checkboxSeleccionados.length < 1) {

                    Swal.fire('Por favor selecciona uno o mas items', '',
                        'warning')
                } else {
                    var contador = 0;
                    var arrTodo = new Array();
                    var validar = true;
                    for (let index = 0; index < checkboxSeleccionados.length; index++) {
                        contador++;
                        var item = {};
                        item = checkboxSeleccionados[index];
                        arrTodo.push(item);
                        if (item == "NO") {
                            validar = false;
                        }
                    }

                    if (validar == true) {

                        Swal.fire('Se quito la asignacion a ' + contador + ' items exitosamente ', '',
                            'success').then((result) => {
                            location.reload();
                        })

                    } else {

                        Swal.fire('Se encontró un item con garantía no asignada, por favor vuelve a intentarlo', '',
                            'warning')
                    }
                }
            }

            function tipoGarantiaSelect(option) {
                var select;
                switch (option) {
                    case option = '1':

                        var select =
                            "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>Autorización de cambio</option> <option>Generación de nota crédito</option> <option>Emisión factura</option> <option>Caso cerrado</option> </select>";

                        break;
                    case option = '2':

                        var select =
                            "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>En espera de repuestos</option> <option>En reparación</option><option>Reparación concluida</option><option>Caso cerrado</option></select>";

                        break;
                    case option = '3':
                        var select =
                            "<label for='inputEmail4'>Estado de Solicitud</label><select class='form-control'><option>Autorización nota crédito</option> <option> Nota crédito emitida</option><option>Caso cerrado</option></select>";

                    default:
                        break;
                }

                $("#estadoSolicitudDiv").attr("disabled", false);
                $("#estadoSolicitudDiv").html(select);
            }

            function abrirModal() {
                $('#exampleModalCenter').modal('show');
            }
        </script>
    @endsection
