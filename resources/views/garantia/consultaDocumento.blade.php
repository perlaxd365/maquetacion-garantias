@extends('layouts.plantilla')
@section('title_page', 'Consulta de Documento')
@section('content_page')


    <?php
    $timestamp = new DateTime(null, new DateTimeZone('America/Lima'));
    $fecha = $timestamp->format('Y-m-d');
    
    ?>
    <div class="card">
        <h5 class="card-header bg-light">Consulta de Facturas</h5>
        <div class="card-body row">

            <div class="card col-md-6">
                <div class="card-header">
                    Fecha de Emisión
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Desde</label>
                        <input type="date" class="form-control date datepicker" id="datepicker" value="<?php echo $fecha; ?>"
                            placeholder="01/02/22">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Hasta</label>
                        <input type="date" class="form-control date datepicker" id="datepicker" placeholder="01/02/22">
                    </div>
                </div>
            </div>
            <div class="card col-md-6">
                <div class="card-header">
                    Datos del documento
                </div>
                <div class="card-body row" style="padding-top: 50px">

                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" id="inputEmail4" placeholder="F001">
                    </div>
                    <div class="form-group col-md-8">
                        <input type="text" class="form-control" id="inputEmail4" placeholder="328778872">
                    </div>
                </div>
            </div>
            <div class="card col-12">
                <div class="card-header">
                    Producto
                </div>
                <div class="card-body row">
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Código</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Descripción</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Serie</label>
                        <input type="text" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                </div>
            </div>



            <div style="padding-left: 20px">
                <button type="submit" class="btn btn-primary">Limpiar</button>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body row">
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Ítem</th>
                        <th scope="col">Documento (Serie-Número)</th>
                        <th scope="col">Fecha Emisión</th>
                        <th scope="col">Moneda</th>
                        <th scope="col">Importe</th>
                        <th scope="col">Total Item</th>
                        <th scope="col">Ver Detalle</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>F001-00007676</td>
                        <td>03/04/2022</td>
                        <td>SOLES</td>
                        <td>5000</td>
                        <td>1</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                ...
                            </button></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>F002-000023322</td>
                        <td>01/04/2022</td>
                        <td>SOLES</td>
                        <td>3000</td>
                        <td>3</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                ...
                            </button></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>F003-000023677</td>
                        <td>06/04/2022</td>
                        <td>DOLARES</td>
                        <td>1000</td>
                        <td>1</td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#exampleModalCenter">
                                ...
                            </button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>



    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detalle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">




                    <table class="table table-responsive table-hover">
                        <thead class="thead-dark">
                            <tr style="hover:">
                                <th scope="col"></th>
                                <th scope="col">Fecha Despacho</th>
                                <th scope="col">Documento Depacho</th>
                                <th scope="col">Fecha Límite Garantía</th>
                                <th scope="col">Documento (Serie-Número)</th>
                                <th scope="col">Codigo Producto</th>
                                <th scope="col">Descripción Producto</th>
                                <th scope="col">Serie</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="radio" name="selected"></td>
                                <td>11/04/2022</td>
                                <td>0000124123</td>
                                <td>11/07/2022</td>
                                <td>001-29300102</td>
                                <td>PRO-001</td>
                                <td>Laptop Dell Ryzen 5 15.6" 8GB 256GB SSD</td>
                                <td>S-41-1</td>
                            </tr>
                            <tr>
                                <td><input type="radio" name="selected"></td>
                                <td>10/04/2022</td>
                                <td>0000124198</td>
                                <td>19/07/2022</td>
                                <td>001-29300102</td>
                                <td>PRO-002</td>
                                <td>Laptop Lenovo 20.1" 12GB 500GB SSD</td>
                                <td>S-41-1</td>
                            </tr>

                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <button type="button" onclick="EnviarForm()" ; class="btn btn-primary">Aplicar Garantía</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function EnviarForm() {
                Swal.fire({
                    title: 'Confirmar Garantía',
                    text: '¿Desea aplicar la garantía al producto seleccionado?',
                    type: 'warning',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Aplicar',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    window.location = "{{ url('garantia/detalleSolicitud') }}";
                })
            }
        </script>
    @endsection
