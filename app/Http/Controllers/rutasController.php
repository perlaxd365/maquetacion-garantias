<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF; // at the top of the file

class rutasController extends Controller
{
    public function registroSolicitudController(){
        return view('garantia.registroSolicitud');
    }
    public function consultaDocumentoController(){
        return view('garantia.consultaDocumento');
    }
    public function consultaSolicitudController(){
        return view('garantia.consultaSolicitud');
    }
    public function detalleSolicitudController(){
        return view('garantia.detalleSolicitud');
    }
    public function consultaSolicitudAsignarController(){
        return view('garantia.consultaSolicitudAsignar');
    }
    public function informeTecnicoController(){
        return view('garantia.informeTecnico');
    }
    public function garantiaExternaController(){
        return view('garantia.garantiaExterna');
    }
    public function consultaSolicitudEjecutarController(){
        return view('garantia.consultaSolicitudEjecutar');
    }
    public function plantillapdfController(){
        return view('garantia.reporteInformeTecnico');
    }
    public function pdfController(){
        $view = \View::make('garantia.reporteInformeTecnico');

        $html=$view->render();
        PDF::SetTitle('Reporte Garantías');
        PDF::AddPage('L');
        PDF::WriteHTML($html,true,true,false,false,'');
        PDF::Output(uniqid().'InformeTecnico.pdf','D');
        PDF::Output(public_path(uniqid().'InformeTecnico.pdf'),'F');
    }
    public function estadoAtencionViewController(){
        return view('garantia.estadoAtencionView');
    }
    public function reporteFallasViewController(){
        return view('garantia.reporteFallasView');
    }
    public function reporteFallasPdfController(){
        $view = \View::make('garantia.reporteFallasPdf');

        $html=$view->render();
        PDF::SetTitle('Reporte Fallas');
        PDF::AddPage();
        PDF::WriteHTML($html,true,true,false,false,'');
        PDF::Output(uniqid().'Fallas.pdf','D');
        PDF::Output(public_path(uniqid().'Fallas.pdf'),'F');
    }
}
