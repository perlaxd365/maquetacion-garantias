<?php

use App\Http\Controllers\rutasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum',config('jetstream.auth_session'),'verified'])->group(function () {
    Route::get('/dash', function () {
        return view('dash.index');
    })->name('dash');
    Route::get('/home', function () {
        return view('dash.index');
    })->name('dash');
});

Route::controller(rutasController::class)->group(function(){
    Route::get("garantia/registroSolicitud","registroSolicitudController");
    Route::get("garantia/consultaDocumento","consultaDocumentoController");
    Route::get("garantia/consultaSolicitud","consultaSolicitudController");
    Route::get("garantia/detalleSolicitud","detalleSolicitudController");
    Route::get("garantia/consultaSolicitudAsignar","consultaSolicitudAsignarController");
    Route::get("garantia/informeTecnico","informeTecnicoController");
    Route::get("garantia/garantiaExterna","garantiaExternaController");
    Route::get("garantia/consultaSolicitudEjecutar","consultaSolicitudEjecutarController")->name('garantia.consultaSolicitudEjecutar');
    Route::get("garantia/pdf","pdfController")->name('DownloadPdf');
    Route::get("garantia/reporteInformeTecnico","plantillapdfController")->name('htmlPdf');
    Route::get("garantia/reporteAveria","reporteAveriasController")->name('reportesAveria');
    Route::get("garantia/estadoAtencion","estadoAtencionViewController")->name('estadoAtencion');
    Route::get("garantia/reporteFallas","reporteFallasViewController")->name('reporteFallas');
    Route::get("garantia/reporteFallasPDF","reporteFallasPdfController")->name('reporteFallasPDF');
});